import 'package:flutter/material.dart';

class PlaceData {
  final String km;
  final String imgtag;
  final String date;
  final String title;

  PlaceData({this.km, this.imgtag, this.date, this.title});
}

List<PlaceData> placeData() {
  return <PlaceData>[
    PlaceData(
        date: 'Haifa * March 10, 2021',
        title: 'Eravikulam National Park',
        imgtag: 'assets/images/Enp.jpg',
        km: '2.4 km'),
    PlaceData(
        date: 'Haifa * March 10, 2021',
        title: 'Football tournament',
        imgtag: 'assets/images/fooball.jpg',
        km: '2.4 km'),
    PlaceData(
        date: 'Haifa * March 10, 2021',
        title: 'Kunnamkulam National Park',
        imgtag: 'assets/images/Enp.jpg',
        km: '2.4 km'),
    PlaceData(
        date: 'Haifa * March 10, 2021',
        title: 'Football tournament',
        imgtag: 'assets/images/Enp.jpg',
        km: '2.4 km')
  ];
}

class PlaceShow extends StatelessWidget {
  final String images;
  final String kms;
  final String title;
  final String date;

  const PlaceShow({Key key, this.images, this.kms, this.title, this.date})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Stack(
        alignment: Alignment.topRight,
        children: [
          Container(
            width: 200,
            height: 280,
            //borderRadius: BorderRadius.all(Radius.circular(10)),

            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(9))),
            clipBehavior: Clip.antiAlias,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset(
                  images,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      CircleAvatar(
                        backgroundImage: AssetImage(images),
                        radius: 8,
                      ),
                      SizedBox(width: 10),
                      Text(
                        date,
                        style: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(
                    title,
                    softWrap: true,
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 15),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      CircleAvatar(
                        backgroundImage: AssetImage(images),
                        radius: 10,
                      ),
                      Spacer(),
                      Row(
                        children: [
                          Icon(
                            Icons.star,
                            color: Colors.deepOrange,
                            size: 18,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.deepOrange,
                            size: 18,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.deepOrange,
                            size: 18,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.deepOrange,
                            size: 18,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.deepOrange,
                            size: 18,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Row(
                    children: [
                      Icon(
                        Icons.person_add_outlined,
                        size: 18,
                      ),
                      SizedBox(width: 10),
                      Text(
                        '3 attending',
                        style: TextStyle(fontSize: 14),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              padding: EdgeInsets.all(3),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(15),
                ),
              ),
              child: Row(
                children: [
                  Icon(
                    Icons.location_on,
                    color: Colors.deepOrange,
                    size: 18,
                  ),
                  Text(
                    kms,
                    style: TextStyle(fontWeight: FontWeight.w500),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
